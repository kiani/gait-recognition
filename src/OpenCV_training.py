import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('Data/handprint1.jpg')

px = img[0,0]
print(px)
print('shape. rows: {}, columns: {}, channels: {}'.format(
    img.shape[0], img.shape[1], img.shape[2]
))
print('size: {}'.format(img.size))
print('data type: {}'.format(img.dtype))

laplacian = cv2.Laplacian(img,cv2.CV_64F)
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
sobelxy = cv2.Sobel(img, cv2.CV_64F,1,1,ksize=5)
sobelx_3 = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=3)
sobely_3 = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=3)
sobelxy_3 = cv2.Sobel(img, cv2.CV_64F,1,1,ksize=3)

plt.subplot(3,3,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,2),plt.imshow(sobelxy,cmap = 'gray')
plt.title('Sobel X Y'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,5),plt.imshow(sobelx_3,cmap = 'gray')
plt.title('Sobel X 3x3'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,6),plt.imshow(sobely_3,cmap = 'gray')
plt.title('Sobel Y 3x3'), plt.xticks([]), plt.yticks([])
plt.subplot(3,3,7),plt.imshow(sobelxy_3,cmap = 'gray')
plt.title('Sobel X Y 3x3'), plt.xticks([]), plt.yticks([])

plt.show()