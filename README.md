# Gait Recognition

Bachelor's thesis gait recognition using Thermal Imaging

# Setup Project
1. Clone the repository
git clone https://gitlab2.cip.ifi.lmu.de/kiani/gait-recognition.git

2. Install conda
https://conda.io/projects/conda/en/latest/user-guide/install/index.html

3. Create the conda environment from the yml file
- open conda shell and navigate to the repo directory
- conda env create

4. activate the conda environment 
conda activate bachelor_thesis
