import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
from matplotlib.dates import date2num, datestr2num
from dataset import Dataset
    
def compare_areas_among_sets(area, set_number):
    johannes = Dataset('../data/johannes' + str(set_number) + '.dat')
    sarah = Dataset('../data/sarah' + str(set_number) + '.dat')
    stefan = Dataset('../data/stefan' + str(set_number) + '.dat')
    plt.plot_date(x=johannes.times, y=johannes.data[area] , xdate=True, ydate=False, linestyle="-", c='red', marker=',', label='johannes')
    plt.plot_date(x=sarah.times, y=sarah.data[area] , xdate=True, ydate=False, linestyle="-", c='blue', marker=',', label='sarah')
    plt.plot_date(x=stefan.times, y=stefan.data[area] , xdate=True, ydate=False, linestyle="-", c='green', marker=',', label='stefan')
    plt.legend()
    plt.grid(axis='y', )
    plt.title('Area: ' + area)
    plt.xlabel('Time since start')
    plt.ylabel('° Celcius on avg.')
    plt.show()

def compare_area_with_shoes_within_participant(area, participant):
    dataset1 = Dataset('./data/' + participant + '1.dat')
    dataset2 = Dataset('./data/' + participant + '2.dat')
    dataset3 = Dataset('./data/' + participant + '3.dat')
    dataset4 = Dataset('./data/' + participant + '4.dat')
    dataset5 = Dataset('./data/' + participant + '5.dat')
    plt.plot_date(x=dataset1.times, y=dataset1.data[area] , xdate=True, ydate=False, linestyle="-", c='red', marker=',', label='dataset1')
    plt.plot_date(x=dataset2.times, y=dataset2.data[area] , xdate=True, ydate=False, linestyle="-", c='blue', marker=',', label='dataset2')
    plt.plot_date(x=dataset3.times, y=dataset3.data[area] , xdate=True, ydate=False, linestyle="-", c='green', marker=',', label='dataset3')
    plt.plot_date(x=dataset4.times, y=dataset4.data[area] , xdate=True, ydate=False, linestyle="-", c='yellow', marker=',', label='dataset4')
    plt.plot_date(x=dataset5.times, y=dataset5.data[area] , xdate=True, ydate=False, linestyle="-", c='purple', marker=',', label='dataset5')
    plt.legend()
    plt.grid(axis='y', )
    plt.title('Area: ' + area)
    plt.xlabel('Time since start')
    plt.ylabel('° Celcius on avg.')
    plt.show()

compare_area_with_shoes_within_participant()
#compare_areas_among_sets('rightFootHeel', 1)
#compare_areas_among_sets('rightFootMid', 1)
#compare_areas_among_sets('rightFootToes', 1)
#compare_areas_among_sets('leftFootHeel', 1)
#compare_areas_among_sets('leftFootMid', 1)
#compare_areas_among_sets('leftFootToes', 1)
