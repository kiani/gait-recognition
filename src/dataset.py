import numpy as np
from datetime import datetime
from matplotlib.dates import date2num

class Dataset:
    def __init__(self, datapath):
        self.all_data = np.genfromtxt(datapath, skip_header=7, skip_footer=2, dtype=None, delimiter=' ', encoding='bytes')
        self.data = {'rightFootHeel': [], 'rightFootMid': [], 'rightFootToes': [], 'leftFootHeel': [], 'leftFootMid': [], 'leftFootToes': []}
        timesDatetime = []
        for single in self.all_data:
            byte_array = (single.split(b'\t'))
            timesDatetime.append(datetime.strptime(byte_array[0].decode('utf-8'), '%H:%M:%S.%f'))
            self.data['rightFootHeel'].append(float(byte_array[1].decode('utf-8')))
            self.data['rightFootMid'].append(float(byte_array[2].decode('utf-8')))
            self.data['rightFootToes'].append(float(byte_array[3].decode('utf-8')))
            self.data['leftFootHeel'].append(float(byte_array[4].decode('utf-8')))
            self.data['leftFootMid'].append(float(byte_array[5].decode('utf-8')))
            self.data['leftFootToes'].append(float(byte_array[6].decode('utf-8')))
        self.times = date2num(timesDatetime)
